# Bullshit detector

- a caesar cypher decrypter for messages that may or may not be bullshit
- submission for Solidabis Koodihaaste 2019
- list of common syllables in the Finnish language borrowed from http://saaressa.blogspot.com/2010/04/tavulista.html

## Requirements

- Python
- Streamlit

## Usage

`streamlit rut decrypt.py`