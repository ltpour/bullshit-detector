import json
import os
import requests
from requests.exceptions import HTTPError
import streamlit as st

# fetches the list of crypted messages from the server to the file messages.json
def retrieve():
    try:
        secret = requests.get('https://koodihaaste-api.solidabis.com/secret/')
        secret.raise_for_status()
    except HTTPError as http_err:
        print(f'HTTP error: {http_err}')
    except Exception as err:
        print(f'Error: {err}')
    else:    
        try:        
            messages = requests.get(secret.json()["bullshitUrl"], headers={'Authorization': 'Bearer ' + secret.json()["jwtToken"]})
            messages.raise_for_status()
        except HTTPError as http_err:
            print(f'HTTP error: {http_err}')
        except Exception as err:
            print(f'Error: {err}')
        else:
            with open('messages.json', 'w') as json_file:
                json.dump(messages.json(), json_file)

# returns a message with each letter shifted right by a given number of places.
# assumes the Finnish alphabet.
def caesar(message, cypher):
    message = list(message)
    for i in range(len(message)):
        c = ord(message[i])
        # temporary values for non-ascii letters
        if c == 229:
            c = 123
        elif c == 228:
            c = 124
        elif c == 246:
            c = 125
        elif c == 197:
            c = 91
        elif c == 196:
            c = 92
        elif c == 214:
            c = 93
        # shift right and rotate if necessary
        if c > 64 and c < 94: # capital letters            
            c += cypher
            if c > 93:
                c -= 29
        elif c > 96 and c < 126: # small letters            
            c += cypher
            if c > 125:
                c -= 29
        # restore correct values for non-ascii letters
        if c == 123:
            c = 229
        elif c == 124:
            c = 228
        elif c == 125:
            c = 246
        elif c == 91:
            c = 197
        elif c == 92:
            c = 196
        elif c == 93:
            c = 214      
        message[i] = chr(c)            
    return "".join(message)

# takes a potentially decrypted message and one or more lists of syllables and
# returns a weighted ratio of given syllables relative to the length of the message.
# the first list of syllables should contain the most common syllables in the target language,
# while the rest of the lists (if any) should contain increasingly uncommon syllables.
def evaluate(message, syllablelists):
    n = 0
    for i in range(len(syllablelists)):
        for syllable in syllablelists[i]:
            n += message.count(syllable) / pow(2, i)
    return n / len(message)

# returns a tuple of the result (0 for likely bullshit, 1 for likely not bullshit) and
# the message itself (unaltered if likely bullshit, decrypted if likely not bullshit)
def solve(message, syllablelists, threshold = 0.9):
    for i in range (0, 29):
        if evaluate(caesar(message, i), syllablelists) > threshold:
            return (1, caesar(message, i))
    return (0, message)

# returns the list of crypted messages from the file messages.json
@st.cache
def get_messages():
    if not os.path.isfile('./static/messages.json'):
        retrieve()
    with open('messages.json') as f:
        bullshits = json.load(f)
    return [message['message'] for message in [bullshit for bullshit in bullshits['bullshits']]]

# returns the list of common syllables in the Finnish language from the file syllables.txt
@st.cache
def get_syllables():
    with open('syllables.txt', encoding='latin1') as f:
        syllables = f.readline().strip().split(',')
    return syllables

def main():
    st.title("Bullshit detector")

    # user-adjustable threshold for the required ratio of detected syllables.
    # values around 0.9 yield the best results for the test data with the included syllable list.
    # when using multiple syllable lists, higher values are likely to be necessary.
    threshold = st.slider('Threshold', 0.0, 5.0, 0.9, 0.01)
    
    messages = get_messages()
    syllables = get_syllables()
    solved = []
    unsolved = []

    # attempt to solve decrypted messages
    for m in messages:
        solution = solve(m, [syllables], threshold)
        if solution[0] == 1:
            solved.append(solution[1])
        else:
            unsolved.append(solution[1])

    # display results
    st.header("Likely not bullshit")
    for s in solved:
        st.write(s)        
    st.header("Likely bullshit")
    for s in unsolved:
        st.write(s)        
    st.header("Summary")
    st.write("{0:.2f}".format(threshold) + " threshold")
    st.write(str(len(solved)) + " likely not bullshit messages")
    st.write(str(len(unsolved)) + " likely bullshit messages")
    
if __name__== "__main__":
    main()
